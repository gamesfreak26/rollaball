﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpeedPickup : MonoBehaviour {

    public event Action<float> SpeedPickupObjectHandler;

    [SerializeField] private float _speed;

    public float Speed {
        get { return _speed; }
        private set { _speed = value; }
    }

    void OnTriggerEnter(Collider other) {
        if (other.CompareTag("Player")) {
            if (SpeedPickupObjectHandler != null) {
                SpeedPickupObjectHandler(5f);
                Destroy(gameObject);
            }
        }
    }
}
