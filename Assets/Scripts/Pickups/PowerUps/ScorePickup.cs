﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = System.Random;

public delegate void PickedUpPickupObjectHandler();

public class ScorePickup : MonoBehaviour
{
    public event Action<int> PickUpObject;

    void OnTriggerEnter(Collider other) {

        if (other.CompareTag("Player")) {
            if (PickUpObject != null) {
                PickUpObject(10);
                Destroy(gameObject);
            }
        }
    }
}
