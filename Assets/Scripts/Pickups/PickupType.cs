﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum PickupType {
    None,
    Score,
    SpeedUp,
    SlowDown,
    Grow,
    Shrink
}