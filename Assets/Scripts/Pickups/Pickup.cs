﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public delegate void PickupObjectHandler();

public class Pickup : MonoBehaviour {
    [SerializeField] private PickupType pickupTypeType;

    public PickupType PickupType {
        get { return pickupTypeType; }
    }

    public event Action<PickupType> PickUpObject;

    // Start is called before the first frame update
    void OnTriggerEnter(Collider other) {

        if (other.CompareTag("Player")) {
            if (PickUpObject != null) {
                PickUpObject(pickupTypeType);
                Destroy(gameObject);
            }
        }
    }
}
