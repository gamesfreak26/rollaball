﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileController : Shape, IKillabe {

    //private float _projectileSpeed;
    //private Vector2 _projectileDirection;

    // Can't use this because it's private set
    //public float ProjectileSpeed {
    //    get { return _projectileSpeed; }
    //    set { _projectileSpeed = value;  }
    //}

    //public Vector2 ProjectileDirection {
    //    get { return _projectileDirection; }
    //    set { _projectileDirection = value; }
    //}

    public Vector3 ProjectileDirection { get; set; }
    public float ProjectileSpeed { get; set; }

    // Start is called before the first frame update
    protected override void Start() {
        base.Start();

        Name = "Projectile";

        SetColor(0, 255, 0, 255);
        Debug.Log("Projectile Spawned");
    }

    // Update is called once per frame
    void Update() {
        MoveProjectile();
    }

    private void MoveProjectile() {
        transform.Translate(ProjectileDirection * Time.deltaTime * ProjectileSpeed, 
            Space.World);

        var top = transform.position.y + halfHeight;
        if (top > gameSceneController.ScreenBounds.y) {
            gameSceneController.KillObject(this);
        }
    }

    public void Kill() {
        Destroy(gameObject);
    }

    public string GetName() {
        return name;
    }
}
