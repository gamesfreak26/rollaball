﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public delegate void TextOutputHandler(string text);

public class GameSceneController : MonoBehaviour
{
    //[SerializeField] private Vector3 screenbounds;

    public Vector3 ScreenBounds { get; private set; }
    [Range(5f, 20f)] 
    [SerializeField] private float playerSpeed;
    [SerializeField] private EnemyController enemyPrefab;

    private HUDController hudController;
    private int totalPoints;

    private PlayerControllerBTB player;

    // Lambda
    public float PlayerSpeed {
        get => playerSpeed;
        set => playerSpeed = value;
    }

    // private float _playerSpeed;
    /*
     *      public float playerSpeed {
     *          get {
     *              return _playerSpeed;
     *          }
     *          set {
     *              _playerSpeed = value;
     *          }
     *      }
     */


    // Start is called before the first frame update
    void Start()
    {
        playerSpeed = 10f;
        ScreenBounds = GetScreenBounds();
        hudController = FindObjectOfType<HUDController>();
        StartCoroutine(SpawnEnemies());
        player = FindObjectOfType<PlayerControllerBTB>();
    }

    // Update is called once per frame
    void Update() {
        
        if (Input.GetKeyDown(KeyCode.Space)) {
            player.SetColor(Color.red);
        }
        else if(Input.GetKeyUp(KeyCode.Space)) {
            player.SetColor(Color.yellow);
        }
    }

    private Vector3 GetScreenBounds() {
        var camera = Camera.main;
        var screenVector = new Vector3(Screen.width, Screen.height, camera.transform.position.z);
        
        return camera.ScreenToWorldPoint(screenVector);
    }

    private IEnumerator SpawnEnemies() {
        WaitForSeconds wait = new WaitForSeconds(2);
        while (true) {
            float horizontalPostion = Random.Range(-ScreenBounds.x, ScreenBounds.x);
            Vector2 spawnPosition = new Vector2(horizontalPostion, ScreenBounds.y);

            EnemyController enemy = Instantiate(enemyPrefab, spawnPosition, Quaternion.identity);
            
            enemy.EnemyEscaped += EnemyAtBottom;
            enemy.EnemyKilled += EnemyKilled;

            yield return wait;
        }
    }

    private void EnemyKilled(int pointValue) {
        totalPoints += pointValue;
        hudController.ScoreText.text = totalPoints.ToString();
    }

    public void EnemyAtBottom(EnemyController enemy) {
        Destroy(enemy.gameObject);
        Debug.Log("Enemy escaped");
    }

    public void KillObject(IKillabe killable) {
        //Debug.LogWarningFormat("{0} killed by Game Scene Controller", killable.GetName());
        killable.Kill();
    }

    public void OutputText(string output) {
        Debug.Log($"{output} output by GameSceneController");
    }
}
