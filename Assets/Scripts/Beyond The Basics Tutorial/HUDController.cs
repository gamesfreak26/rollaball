﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HUDController : MonoBehaviour {

    [SerializeField] private Text _scoreText;
    [SerializeField] private Text pauseButtonText;

    public Text ScoreText {
        get { return _scoreText; }
        set { _scoreText = value; }
    }

    public void PauseButtonClicked() {
        if (Time.timeScale > 0) {
            Time.timeScale = 0;
            pauseButtonText.text = "Resume";
        }
        else {
            Time.timeScale = 1;
            pauseButtonText.text = "Pause";
        }
    }
}
