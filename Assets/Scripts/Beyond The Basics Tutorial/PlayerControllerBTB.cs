﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControllerBTB : Shape
{
    //private GameSceneController gameSceneController;
    public ProjectileController projectileprefab;

    //void Start() {
    //    gameSceneController = FindObjectOfType<GameSceneController>();

    //    SetColor(Color.yellow);
    //}

    void Awake() {
        //Debug.Log(gameSceneController.ScreenBounds);
    }

    protected override void Start() {
        base.Start();
        SetColor(Color.yellow);
    }

    void Update() {
        MovePlayer();
        if (Input.GetKeyDown(KeyCode.Space)) {
            FireProjectile();
        }

        
    }


    private void MovePlayer() {
        float horizontalMovement = Input.GetAxis("Horizontal");

        if (Mathf.Abs(horizontalMovement) > Mathf.Epsilon) {
            horizontalMovement = horizontalMovement * Time.deltaTime * gameSceneController.PlayerSpeed;
            horizontalMovement += transform.position.x;

            var right = gameSceneController.ScreenBounds.x - halfWidth;
            var left = -right;
            var limit = Mathf.Clamp(horizontalMovement, left, right);

            transform.position = new Vector2(limit, transform.position.y);
        }
    }

    private void FireProjectile() {
        Vector2 spawnPosition = transform.position;

        ProjectileController projectile = Instantiate(projectileprefab, spawnPosition, Quaternion.identity);
        projectile.ProjectileSpeed = 2f;
        projectile.ProjectileDirection = Vector2.up;
    }
}
