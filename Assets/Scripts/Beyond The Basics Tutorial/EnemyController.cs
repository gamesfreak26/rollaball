﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;
using System;

public delegate void EnemyEscapedHandler(EnemyController enemy);

public class EnemyController : Shape, IKillabe {

    public event EnemyEscapedHandler EnemyEscaped;

    public event Action<int> EnemyKilled;

    // Start is called before the first frame update
    protected override void Start() {
        base.Start();

        Name = "Enemy";
    }

    // Update is called once per frame
    void Update() {
        MoveEnemy();
    }

    void OnCollisionEnter2D(Collision2D other) {
        if (EnemyKilled != null) {
            EnemyKilled(10);
            Destroy(other.gameObject);
            Destroy(gameObject);
        }
    }

    private void MoveEnemy() {
        transform.Translate(Vector3.down * Time.deltaTime, Space.World);

        var bottom = transform.position.y - halfHeight;

        if (bottom <= -gameSceneController.ScreenBounds.y) {
            // Simplified Code.  
            //EnemyEscaped?.Invoke(this); 
            if (EnemyEscaped != null) {
                EnemyEscaped(this);
                
            }
        }
    }

    private void InternalOutputText(string output) {
        Debug.Log($"{output} output by EnemyController");
    }

    public void Kill() {
        Destroy(gameObject);
    }

    public string GetName() {
        return name;
    }
}
