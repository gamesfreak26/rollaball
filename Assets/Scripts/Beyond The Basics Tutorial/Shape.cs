﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shape : MonoBehaviour
{
    public string Name;

    protected GameSceneController gameSceneController;
    protected float halfWidth, halfHeight;

    private SpriteRenderer spriteRenderer;

    protected virtual void Start() {
        gameSceneController = FindObjectOfType<GameSceneController>();
        spriteRenderer = GetComponent<SpriteRenderer>();

        halfWidth = spriteRenderer.bounds.extents.x;
        halfHeight = spriteRenderer.bounds.extents.y;
    }

    public void SetColor(Color newColor) {
        spriteRenderer.color = newColor;
    }

    public void SetColor(float r, float g, float b, float a) {
        Color newColor = new Color(r, g, b, a);
        spriteRenderer.color = newColor;
    }
}
