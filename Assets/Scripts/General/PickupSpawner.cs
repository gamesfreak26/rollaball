﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;
using Random = System.Random;

public class PickupSpawner : MonoBehaviour {

    [SerializeField] private Pickup[] pickups;
    [SerializeField] private PlayerController player;
    [SerializeField] private ViewController view;

    private int totalPoints;
    private Random random = new Random();
    private List<ScorePickup> pickupObjectsList = new List<ScorePickup>();

    // Start is called before the first frame update
    void Start() {
        totalPoints = 0;
        
        StartCoroutine("SpawnPickupAtRandomLocation");
    }

    private IEnumerator SpawnPickupAtRandomLocation() {
        WaitForSeconds wait = new WaitForSeconds(2);

        while (true) {
            var horizontolPostion = random.Next(-9, 9);
            var verticalPostion = random.Next(-9, 9);
            var randomPickup = random.Next(0, pickups.Length);

            var pickup = Instantiate(
                pickups[randomPickup],
                new Vector3(horizontolPostion, 0.5f, verticalPostion),
                Quaternion.identity);

            pickup.PickUpObject += OnPickup;

            yield return wait;
        }
    }

    private void OnPickup(PickupType pickupType) {
        switch (pickupType) {
            case PickupType.None:
                break;
            case PickupType.Score:
                AddScore(10);
                break;
            case PickupType.SpeedUp:
                SpeedUpPlayer(2);
                break;
            case PickupType.SlowDown:
                break;
            case PickupType.Grow:
                break;
            case PickupType.Shrink:
                break;
        }
    }

    private void AddScore(int pointValue) {
        Debug.Log("AddScore Raised");
        totalPoints += pointValue;
        view.ScoreText.text = $"Score: {totalPoints}";
    }

    private void SpeedUpPlayer(float speed) {
        Debug.Log("Speed Player Event Raised");
        player.speedPickupTime = 5f;
        player.Speed += speed;
    }
}
