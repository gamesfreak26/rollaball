﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    [SerializeField] private GameObject playerGameObject;
    private Vector3 offset;

    // Start is called before the first frame update
    void Start() {
        offset = transform.position - playerGameObject.transform.position;
    }

    // Update is called once per frame.  LateUpdate similar to update but runs later.
    void LateUpdate() {
        transform.position = playerGameObject.transform.position + offset;
    }
}
