﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    private Rigidbody rigidbody;
    private float initialSpeed;
    public float speedPickupTime = -1;

    [Range(0f, 10f)] 
    [SerializeField] private float _speed;

    public float Speed {
        get { return _speed; }
        set { _speed = value; }
    }

    void Start() {
        rigidbody = GetComponent<Rigidbody>();
        initialSpeed = _speed;
    }

    void FixedUpdate() {
        var moveHorizontal = Input.GetAxis("Horizontal");
        var moveVertical = Input.GetAxis("Vertical");

        var movement = new Vector3(moveHorizontal, 0, moveVertical);

        //if (speedPickupTime > 0) {
        //    Debug.Log("Speed ScorePickup");
        //    rigidbody.AddForce(movement * Speed);
        //    speedPickupTime -= Time.deltaTime;
        //}
        //else {
        //    Debug.Log("Initial Speed");
        //    Speed = initialSpeed;
        //    rigidbody.AddForce(movement * Speed);
        //}

        rigidbody.AddForce(movement * Speed);
    }


    private void IncreaseSpeed() {
        Debug.Log("Speed Increase");
    }
}
