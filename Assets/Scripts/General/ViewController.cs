﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ViewController : MonoBehaviour {
    [SerializeField] private Text _scoreText;
    [SerializeField] private Text _winText;

    public Text ScoreText {
        get { return _scoreText; }
        set { _scoreText = value; }
    }

    public Text WinText {
        get { return _winText; }
        set { _winText = value; }
    }
}
